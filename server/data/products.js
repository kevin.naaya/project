const products = [
    {
        
        name : "Living Room Sofa",
        image: 'https://images.unsplash.com/photo-1555041469-a586c61ea9bc?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1770&q=80',
        description: "Sofa",
        price: 12000,
        countInStock: 100,
        ratings: 4.6,
        numReviews: 5,
        category: "Furniture",
        brand: "Demo",
    },
    {
       
        name : "Water Bottle",
        image: '/Image/bottle.jpg',
        description: "Water bottle for kids",
        price: 12000,
        countInStock: 100,
        ratings: 4.6,
        numReviews: 5,
        category: "Water bottle",
        brand: "demo",
    },
    {
        
        name : "Nike Shoes",
        image: '/Image/nj.jpg', 
        description: "Sofa",
        price: 12000,
        countInStock: 100,
        ratings: 4.6,
        numReviews: 5,
        category: "Footwear",
        brand: "demo",
    },
    {
    
        name: "Round Neck T-Shirts",
        image: '/Image/male_img_4.jpg',
        description: "A round neck cotton tshirt",
        brand: "Urban OutFitters",
        price: 899,
        countInStock: 42,
        rating: 4.1,
        numReviews: 2,
        category: "Men Fashion",
    },
    {
        
        name: "Juventus Henley Neck Jersey",
        image: "/Image/female_img_1.jpg",
        description: "A tshirt for women",
        brand: "Tom Ford",
        category: "Female Fashion",
        price: 3999,
        countInStock: 40,
        rating: 3.5,
        numReviews: 4,
    },
];

export default products;
import express from 'express';

import { authUser,
         getUserProfile,
         registerUser,
         getUsers,
         updateUserProfile,
         verifyUser,
         deleteUser,
         getUserByID,
         updateUser,
        } from '../controllers/userController.js';
import { superAdmin, protect } from '../middlewares/authMiddleware.js';

const router = express.Router();

router.route('/').post(registerUser).get(protect, superAdmin, getUsers);

router.route('/login').post(authUser);

router.route('/profile').get(protect, getUserProfile)
.put(protect, updateUserProfile);

router.get('/verify', verifyUser);

router.route(':id')
.delete(protect, superAdmin, deleteUser)
.get(protect, superAdmin, getUserByID)
.put(protect, superAdmin, updateUser);

export default router;
import asyncHandler from "express-async-handler";

import Product from "../models/productModel.js";

/**
 * @desc    GET all products
 * @route   Get /api/products
 * @access  public
 */

const getProducts = asyncHandler(async (req, res) => {
  const products = await Product.find({});
  res.json(products);
});

export { getProducts };

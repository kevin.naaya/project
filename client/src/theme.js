import { createTheme } from "@mui/material";

const theme = createTheme({
  palette: {
    primary: {
      main: "#d9d9d9",
      light: "#e9e9e9",
      dark: "#c4c4c4",
      //   contrastText: "#000000",
    },
    secondary: {
      main: "#1470b7",
      light: "#19a2ee",
      dark: "#115194",
      //   contrastText: "",
    },
    tertiary: {
      main: "#3bcfcf",
      light: "#aeebe8",
      dark: "#00abb0",
      //   contrastText: "",
    },
    black: "#000000",
    white: "#ffffff",
  },
  components: {
    MuiButtonBase: {
      defaultProps: {
        disableRipple: true,
      },
    },
  },
  typography: {
    fontFamily: "'Mukta', sans-serif",
  },
});

export default theme;
